FROM node:10.13.0-alpine

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Install app dependencies
RUN apk update && apk upgrade && apk add git

COPY . /usr/src/app/
RUN npm install
RUN npm run build

EXPOSE 3001

# start command
CMD [ "npm", "run", "prod" ]