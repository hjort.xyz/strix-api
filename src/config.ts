
import secretConfig from './secrets';
const rawEnv: any = process.env['NODE_ENV'];
const env: any = rawEnv.trim() || 'development';

interface config {
	db: string,
	webapp: string,
	port: number,
	env: string,
	secret: string
};

let config: config;

if (env === 'production') {
	config = {
		db: 'mongodb://mongo:27017/strix',
		webapp: 'https://strix.hjort.xyz', // For CORS
		port: 3001,
		env: env,
		secret: secretConfig.secret
	};
} else {
	config = {
		db: 'mongodb://localhost:27017/strix',
		webapp: 'http://localhost:3000', // For CORS
		port: 3001,
		env: env,
		secret: secretConfig.secret
	};
};

export default config;
