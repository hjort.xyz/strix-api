export default {
	"contacts": [
		{
			"firstname": "Emil",
			"middlename": "Oehlenschlæger",
			"lastname": "Hjort",
			"phone_numbers": [
				{
					"country_calling_code": "45",
					"number": "28514103"
				}
			],
			"emails": [
				"emil.oehh.hjort@gmail.com"
			],
			"birthdate": "May 26, 1994 00:00:00",
			"other_services": []
		}
	],
	"notes": [
		{
			"body": "The best place to breathe",
			"title": "Anywhere theres air \n like earth"
		},
		{
			"body": "The best place to drink water",
			"title": "Anywhere theres water \n like earth"
		}
	],
	"tasks": [
		{
			"title": "Buy milk",
			"description": "2 litres"
		},
		{
			"title": "Buy egg"
		},
		{
			"title": "Buy dogs",
			"description": "2 dogs"
		},
		{
			"title": "Buy fish",
			"completedAt": 'January 1, 1970 00:00:00'
		}
	]
}
