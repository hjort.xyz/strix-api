import mongoose   from 'mongoose';
import User       from'../../mongoose/user.model';
import test_users from './users.json.js';

mongoose.Promise = global.Promise;

const log = console.log;
const exit_with_error = function (msg: string, err: string) {
	console.error(msg, err);
	log('Exiting...');
	process.exit();
};

async function seed () {
	// Connect to DB
	try {
		await mongoose.connect('mongodb://localhost:27017/strix')
	} catch (err) {
		exit_with_error('Connection error:', err);
	}
	log('MongoDB is connected...');

	// Clear user collection
	try {
		await User.deleteMany({});
	} catch (err) {
		exit_with_error('Could not clear collection:', err);
	}
	log('Collection cleared...');

	// Insert test users
	const user_promises = [];
	for (let user_data of test_users) {
		const user = new User(user_data);
		user_promises.push(user.save());
	}

	try {
		await Promise.all(user_promises);
	} catch (err) {
		exit_with_error('Could not seed all users:', err);
	}

	log('All users successfully seeded...');
	process.exit();
};

seed();
