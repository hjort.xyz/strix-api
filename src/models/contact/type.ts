import { Syncable } from '../syncable';

type PhoneNumber = {
	country_calling_code: string,
	number: string
}

type Service = {
	name: string,
	value: string
}

type Contact = Syncable & {
	firstname: string
	middlename?: string
	lastname: string
	phone_numbers: Array<PhoneNumber>
	emails: Array<string>
	birthdate?: string
    other_services: Array<Service>
}

export default Contact;