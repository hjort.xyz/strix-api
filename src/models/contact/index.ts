import Contact from './type';
import Contact_handlers from './handlers';

export {
    Contact,
    Contact_handlers
};