export interface Syncable_handlers {
    persister: any
    create: <T>(item: T) => Promise<T>
    get: <T>(id: string, owner: string) => Promise<T>
    get_all: <T>(owner: string) => Promise<Array<T>>
    last_updated: (owner: string) => Promise<number>
    update: <T>(syncable: T) => Promise<T> | never
    delete: (id: string, owner: string) => Promise<void>
    strip: <T>(syncable: any) => T | never
}

export const syncable_handlers: Syncable_handlers = {
    persister: {},

    create: async function <T>(this: Syncable_handlers, syncable: T) {
        const persisted_syncable = <any>await this.persister.create(syncable);
        if (!persisted_syncable) throw new Error('Could not create item');

        return this.strip(persisted_syncable);
    },
    
    get: async function (this: Syncable_handlers, id: string, owner: string) {
        const syncable = <any>await this.persister.findOne({ _id: id, owner });
        if (!syncable) throw new Error('No item found');
        
        return this.strip(syncable);
    },
    
    get_all: async function (this: Syncable_handlers, owner: string) {
        const syncables = <any>await this.persister.find({ owner });
        if (!syncables) throw new Error('No items found');

        return syncables.map(this.strip);
    },
    
    last_updated: async function (this: Syncable_handlers, owner: string) {
        let updated_at_list: any[] = <any>await this.persister.find({ owner }, 'updatedAt');

        updated_at_list = updated_at_list.map((syncable) => new Date(syncable.updatedAt).valueOf());
        updated_at_list.sort((a, b) => b - a);

        return updated_at_list[0];
    },
    
    update: async function (this: Syncable_handlers, updated_syncable: any) {
        throw new Error('UPDATE FUNCTION MUST BE OVERWRITTEN BY CHILD MODEL, ITEM: ' + updated_syncable);
    },
    
    delete: async function (this: Syncable_handlers, id: string, owner: string) {
        await this.persister.deleteOne({
            _id: id,
            owner: owner
        });

        return;
    },
    
    strip: function (this: Syncable_handlers, syncable: any) {
        throw new Error('STRIP FUNCTION MUST BE OVERWRITTEN BY CHILD MODEL, ITEM: ' + syncable);
    }
};