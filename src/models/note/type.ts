import { Syncable } from '../syncable';

type Note = Syncable & {
	title: string
	body: string
}

export default Note;