import { Syncable } from '../syncable';

type Task = Syncable & {
	title: string
	description: string
	completedAt?: Date
	cooldownTime?: Date
}

export default Task;