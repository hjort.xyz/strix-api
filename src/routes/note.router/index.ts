import { Router }           from 'express';
import { create_one }       from './create';
import { get_all, get_one } from './get';
import { update_one }       from './update';
import { delete_one }       from './delete';

const note_router: Router = Router();

// GET /note
note_router.get('/', get_all);
note_router.get('/:id', get_one);

// POST /note
note_router.post('/', create_one);

// PATCH /note
note_router.patch('/:id', update_one);

// DELETE /note
note_router.delete('/:id', delete_one);

export default note_router;
