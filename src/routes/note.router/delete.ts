import {
	Request,
	Response
} from 'express';
import {
	Note_handlers
} from '../../models/note';
import log from '../../util/log';

async function delete_one (req: Request, res: Response): Promise<void>  {
	const { id: owner } = req.user;
	const { id } = req.params;
	
	// Delete Note
	try {
		await Note_handlers.delete(id, owner);
		
		// Success response
		res.status(200).end();
	} catch (error) {
		log({ error }, 'error');
		res.status(400).send({ error: 'Could not delete note' });
		return;
	}
};

export {
	delete_one
};