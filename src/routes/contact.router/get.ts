import {
	Request,
	Response
} from 'express';
import {
	Contact_handlers
} from '../../models/contact';
import log from '../../util/log';

async function get_all (req: Request, res: Response): Promise<void> {
	const { id: owner } = req.user;
	
	// Find Contacts
	try {
		const contacts = await Contact_handlers.get_all(owner);
		
		// Success response
		res.json(contacts);
	} catch (error) {
		log({ error }, 'error');
		res.status(400).send({ error: 'Could not find any contacts' });
		return;
	}
};

async function get_one (req: Request, res: Response): Promise<void>  {
	const { id: owner } = req.user;
	const { id } = req.params;

	// Find Contact
	try {
		const contact = await Contact_handlers.get(id as string, owner as string);
		
		// Success response
		res.json(contact);
	} catch (error) {
		log({ error }, 'error');
		res.status(400).send({ error: 'Could not find contact' });
		return;
	}
};

export {
	get_all,
	get_one
};