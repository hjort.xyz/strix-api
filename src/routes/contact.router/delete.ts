import {
	Request,
	Response
} from 'express';
import {
	Contact_handlers
} from '../../models/contact';
import log from '../../util/log';

async function delete_one (req: Request, res: Response): Promise<void>  {
	const { id: owner } = req.user;
	const { id } = req.params;
	
	// Delete Contact
	try {
		await Contact_handlers.delete(id, owner);

		// Success response
		res.status(200).end();
	} catch (error) {
		log({ error }, 'error');
		res.status(400).send({ error: 'Could not delete contact' });
		return;
	}
};

export {
	delete_one
};