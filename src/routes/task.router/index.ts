import { Router }           from 'express';
import { create_one }       from './create';
import { get_all, get_one } from './get';
import { update_one }       from './update';
import { delete_one }       from './delete';

const task_router: Router = Router();

// GET /task
task_router.get('/', get_all);
task_router.get('/:id', get_one);

// POST /task
task_router.post('/', create_one);

// PATCH /task
task_router.patch('/:id', update_one);

// DELETE /task
task_router.delete('/:id', delete_one);

export default task_router;
