import {
	Request,
	Response
} from 'express';
import {
	Task,
	Task_handlers
} from '../../models/task';
import log from '../../util/log';

async function create_one (req: Request, res: Response): Promise<void>  {
	const { id: owner } = req.user;
	const {
		title,
		description
	} = req.body;

	// Create task
	try {
		const task: Task = await Task_handlers.create({
			id: undefined,
			owner,
			has_access: [],
			deleted: false,
			title,
			description,
			completedAt: undefined,
			cooldownTime: undefined,
			hash: undefined
		});
		if (!task) throw new Error('Could not create task');
		
		// Success response
		res.json(task);
	} catch (error) {
		log({ error }, 'error');
		res.status(400).send({ error: 'Could not create task' });
		return;
	}
};

export {
	create_one
};
