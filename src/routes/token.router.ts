import {
	Request,
	Response,
	Router
} from 'express';
import {
	User,
	get_user,
	verify_user_password
} from '../models/user';
import jwt    from 'jsonwebtoken'
import config from '../config'
import log    from '../util/log';

const token_router = Router()

// POST /token
token_router.post('/', async function(req: Request, res: Response) {
	let { username, password } = req.body;

	// Find user
	try {
		const user: User = await get_user(username, 'username');
		if (!user) throw new Error('No user found');
		
		// Check that the entered password is correct
		const match: Boolean = await verify_user_password(user.id, password);
		if (!match)  throw new Error('Authentication failed');
	
		// Create and sign JWT
		const token = jwt.sign(
			user,
			config.secret,
			{}
		);
		
		// Success response
		res.json({ token });
	} catch (error) {
		log({ error }, 'error');
		res.status(401).send({ error: 'Authentication failed' });
		return;
	}
});

export default token_router;
