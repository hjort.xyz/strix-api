import express from 'express';
import httpLib from 'http';
const app      = express();
const http     = new httpLib.Server(app);

import morgan      from 'morgan';
import body_parser from 'body-parser';
import jwt         from './middleware/jwt';
import cors        from './middleware/cors';

import mongoose from 'mongoose';
import './mongoose/user.model';
import './mongoose/task.model';
import './mongoose/note.model';
import './mongoose/contact.model';

import token_router    from'./routes/token.router';
import user_router     from'./routes/user.router';
import status_router     from'./routes/status.router';
import contact_router  from'./routes/contact.router';
import note_router     from'./routes/note.router';
import task_router     from'./routes/task.router';

import log    from './util/log';
import config from './config';

// Middleware
app.use(morgan('dev'));
app.use(jwt);
app.use(cors);
app.use(body_parser.json());

// Database
mongoose.Promise = global.Promise;
mongoose.connect(config.db, { useNewUrlParser: true })
	.then((_db: object) => {
		log('Mongoose/MongoDB connected', 'info')
	})
	.catch((err: string) => {
		log({ 'Connection error': err }, 'error')
		process.exit()
	});


// Routes
app.use('/token', token_router);
app.use('/user', user_router);
app.use('/status', status_router);
app.use('/contact', contact_router);
app.use('/note', note_router);
app.use('/task', task_router);

// 404 handler
const FOFhandler = function (_req : any, res : any) {
	res.status(404).send({ message: 'Not found' });
};

app.get('*', FOFhandler);
app.post('*', FOFhandler);
app.put('*', FOFhandler);
app.delete('*', FOFhandler);

// Start listening
http.listen(config.port, () => log(
	{
		message: 'API running',
		ENV: config.env,
		Port: config.port
	},
	'info'
));
