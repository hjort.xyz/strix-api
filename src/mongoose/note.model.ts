import mongoose from "mongoose";
import Syncable from "./syncable.model";

const noteSchema = new mongoose.Schema({
	title: {
		type: String,
		required: true,
		minlength: 1,
		maxlength: 128
	},
	body: {
		type: String,
		required: false,
		maxlength: 2048
	},
	...Syncable.properties
}, {
	timestamps: true,
	...Syncable.options
});


noteSchema.pre('save', Syncable.hasher);

export default mongoose.model("Note", noteSchema);
