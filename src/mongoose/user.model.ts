import mongoose from 'mongoose';
import bcrypt   from 'bcryptjs';
const SALT_WORK_FACTOR = 10;

export type userType = {
	id?: string
	username: string
}

export type userModel = mongoose.Document & userType & {
	simplify(): userType
	compare_password(): boolean
	password: string

};

const userSchema = new mongoose.Schema({
	id: mongoose.Schema.Types.ObjectId,
	username: {
		type: String,
		required: true,
		unique: true,
		lowercase: true,
		maxlength: 128,
	},
	password: {
		type: String,
		required: true,
		minlength: 8,
		maxlength: 64,
		validate: {
			validator: function (v: string) {
				if (!(/[a-zA-Z]/g.test(v))){
					return false
				} else if (!(/\d+/g.test(v))){
					return false
				} else {
					return true;
				}
			},
			message: 'Must contain 1 letter and 1 number'
		}
	}
});

userSchema.pre('save', async function (this: userModel, next: Function) {
	// Skip if password has not been altered
	if (!this.isModified('password')) return next();
	
	// Generate salt for hashing
	let salt;
	try {
		salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
	} catch (err) {
		return next(err);
	}
	
	// Hash the password
	let hash;
	try {        
		hash = await bcrypt.hash(this.password, salt);
	} catch (err) {
		return next(err);
	}
	
	// Attach hash to user and procced
	this.password = hash;
	next();
});

userSchema.methods.compare_password = async function (candidate_password: string): Promise<Boolean> {
	// Compare the passwords and return bool
	let match;
	try {        
		match = await bcrypt.compare(candidate_password, this.password)
		if (!match) throw new Error ('Password do not match')
		else return true;
	} catch (error) {
		return false;
	}
};

export default mongoose.model('User', userSchema);
