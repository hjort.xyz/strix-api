import mongoose from "mongoose";
import Syncable from "./syncable.model";

const contactSchema = new mongoose.Schema({
	firstname: {
		type: String,
		required: true,
		minlength: 1,
		maxlength: 256
	},
	middlename: {
		type: String,
		required: false,
		maxlength: 256
	},
	lastname: {
		type: String,
		required: false,
		maxlength: 256
	},
	phone_numbers: [{
		country_calling_code: {
			type: String,
			required: false,
			minlength: 1,
			maxlength: 3
		},
		number: {
			type: String,
			required: true,
			minlength: 1,
			maxlength: 16
		}
	}],
	emails: [{
		type: String,
		required: true,
		minlength: 1,
		maxlength: 256
	}],
	birthdate: {
		type: Date,
		required: false
	},
	other_services: [{
		name: {
			type: String,
			required: true,
			minlength: 1,
			maxlength: 128
		},
		value: {
			type: String,
			required: true,
			minlength: 1,
			maxlength: 256
		}
	}],
	...Syncable.properties
}, {
	timestamps: true,
	...Syncable.options
});

contactSchema.pre('save', Syncable.hasher);

export default mongoose.model("Contact", contactSchema);

