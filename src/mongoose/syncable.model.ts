import crypto from "crypto";
import mongoose from "mongoose";

const properties = {
	id: mongoose.Schema.Types.ObjectId,
	owner: mongoose.Schema.Types.ObjectId,
	has_access: [mongoose.Schema.Types.ObjectId],
	hash: {
		type: String,
		required: false,
		default: null,
		maxlength: 32,
		set: function(hash: string) {
			const self: any = this
			self._old_hash = self.hash
			return hash;
		}
	},
	deleted: {
		type: Boolean,
		required: false,
		default: false
	},
	schema_version: {
		type: Number,
		required: false,
		default: 1
	}
}

const options = {
	timestamps: true
};

async function hasher (this: any, next: Function) {
	const contact = this.toObject();
	
	if (contact._old_hash !== null && // If null then its a new contact, and we ignore
		contact._old_hash !== contact.hash // If hashes are different we might have update conflict
		) {
		return new Error('Update conflict!')
	}

	// Create an md5 hash
	delete contact._old_hash;
	let hash = crypto.createHash('md5');
	hash.update(JSON.stringify(contact));
	
	// Attach hash to user and procced
	this.hash = hash.digest('hex');
	next();
};

export default {
	properties,
	options,
	hasher
}
